import dlib,cv2
from skimage import io
from numpy import array,int32

### Intializing detectors
predictor_path = 'src/shape_predictor_68_face_landmarks.dat'
DianaImage = 'src/diana.jpg'
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(predictor_path)
### Intializing detectors

### Intializing cvWindow
win = dlib.image_window()
cap = cv2.VideoCapture(0)
### Intializing cvWindow

while(True):
    ### Capture camera frames
    ret, img = cap.read()
    dets = detector(img, 1)
    mouthPoly = []
    for k, d in enumerate(dets):
        shape = predictor(img, d)
        for x in range(48,shape.num_parts):
            shapePos = shape.part(x)
            #MouthParts [48 ~ 68]
            mouthPoly.append([shapePos.x,shapePos.y])
    ### Capture camera frames
    
    ### Masking the mouth
    Maskimg = io.imread(DianaImage)
    #Maskimg = img
    cv2.circle(Maskimg,(0,0), 5000, (0,0,0), -1)
    pts = array([mouthPoly], int32)
    pts = pts.reshape((-1,1,2))
    cv2.fillPoly(Maskimg,[pts],(255,255,255))
    ### Masking the mouth
        
    ### Image with Lips Color
    EdImage = Maskimg
    cv2.fillPoly(EdImage,[pts],(255,0,0))
    ### Image with Lips Color

    ### Compositing LipsColor with camera frames
    img1 = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img2 = EdImage

    rows,cols,channels = img2.shape
    roi = img1[0:rows, 0:cols ]

    img2gray = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)

    img1_bg = cv2.bitwise_and(roi,roi,mask = mask_inv)
    img2_fg = cv2.bitwise_and(img2,img2,mask = mask)

    dst = cv2.addWeighted(img1_bg,1,img2_fg,1,0)
    img1[0:rows, 0:cols ] = dst
    ### Compositing LipsColor with camera frames

    ### Show cvWindow
    win.set_image(img1)
    ### Show cvWindow
